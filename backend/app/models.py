from app import db
from sqlalchemy.orm import relationship


class Guild(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    status = db.Column(db.Boolean)

    def __repr__(self):
        return '<Guild {}>'.format(self.name)


class Conveyor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    status = db.Column(db.Boolean)
    guild_id = db.Column(db.Integer, db.ForeignKey(Guild.id))
    guild = relationship(Guild, uselist=False)

    def __repr__(self):
        return '<Conveyor {}>'.format(self.name)


class Zone(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    status = db.Column(db.Boolean)
    guild_id = db.Column(db.Integer, db.ForeignKey(Guild.id))
    guild = relationship(Guild, uselist=False)

    def __repr__(self):
        return '<Zone {}>'.format(self.name)