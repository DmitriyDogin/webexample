from app import app, db
from app.models import Guild, Conveyor
from flask import render_template, request
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField
from wtforms.validators import DataRequired

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/guild_list', methods=['GET'])
def guild_list():
    guilds = Guild.query.all()
    return render_template('guild_list.html', guilds=guilds)


class GuildForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired], _name='name')
    status = BooleanField('Is active', _name='status')
    submit = SubmitField('Add')


@app.route('/guild_form', methods=['GET'])
def add_guild_form():
    form = GuildForm()
    return render_template('add_guild_form.html', form=form)


@app.route('/guild', methods=['POST'])
def add_guild():
    name = request.form.get('name')
    if request.form.get('status'):
        stat = True
    else:
        stat = False
    guild = Guild(name=name, status=stat)
    db.session.add(guild)
    db.session.commit()
    return render_template('guild.html', guild=guild, message="New guild is added")


def do_change_guild_status(guild):
    guild.status = not guild.status
    db.session.commit()
    return render_template('guild.html', guild=guild, message="Status is changed")


def do_delete_guild(guild):
    name = guild.name
    db.session.delete(guild)
    db.session.commit()
    return render_template('delete_guild.html', name=name)


def do_replace_guid(guild, name, status):
    guild.name = name
    guild.status = status
    db.session.commit()
    return render_template('guild.html', guild=guild, message="Guild is changed")


@app.route('/guild/<string:guild_id>', methods=['GET'])
def get_guild(guild_id):
    guild = Guild.query.get(guild_id)
    if request.args.get('_method') == 'PATCH':
        return do_change_guild_status(guild)
    elif request.args.get('_method') == 'DELETE':
        return do_delete_guild(guild)
    elif request.args.get('_method') == 'PUT':
        name = request.args.get('name')
        if request.args.get('status'):
            stat = True
        else:
            stat = False
        return do_replace_guid(guild, name, stat)
    else:
        conveyor = Conveyor.query.filter(Conveyor.guild == guild).first()
        return render_template('guild.html', guild=guild, message="", conveyor=conveyor)


@app.route('/guild/<string:guild_id>', methods=['PATCH'])
def change_guild(guild_id):
    guild = Guild.query.get(guild_id)
    return do_change_guild_status(guild)


@app.route('/guild/<string:guild_id>', methods=['DELETE'])
def delete_guild(guild_id):
    guild = Guild.query.get(guild_id)
    return do_delete_guild(guild)


@app.route('/guild/<string:guild_id>', methods=['PUT'])
def replace_guild(guild_id):
    guild = Guild.query.get(guild_id)
    name = request.args.get('name')
    if request.args.get('status'):
        stat = True
    else:
        stat = False
    return do_replace_guid(guild, name, stat)


@app.route('/guild/<string:guild_id>/change', methods=['GET'])
def change_guild_form(guild_id):
    g = Guild.query.get(guild_id)
    return render_template('change_guild_form.html', guild=g)



@app.route('/conveyor_list', methods=['GET'])
def conveyor_list():
    conveyors = Conveyor.query.all()
    return render_template('conveyor_list.html', conveyors=conveyors)


class ConveyorForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired], _name='name')
    status = BooleanField('Is active', _name='status')
    submit = SubmitField('Add')


@app.route('/conveyor_form', methods=['GET'])
def add_conveyor_form():
    guild_id = request.args.get('guild_id')
    print (" gg_id = ", guild_id)
    form = ConveyorForm()
    return render_template('add_conveyor_form.html', form=form, guild_id=guild_id)


@app.route('/conveyor', methods=['POST'])
def add_conveyor():
    name = request.form.get('name')
    if request.form.get('status'):
        stat = True
    else:
        stat = False
    guild_id = request.form.get('guild_id')
    guild = Guild.query.get(guild_id)
    conv = Conveyor(name=name, status=stat)
    db.session.add(conv)
    guild.conveyor = conv
    db.session.commit()
    return render_template('guild.html', guild=guild, message="Conveyor is added", conveyor=conv)